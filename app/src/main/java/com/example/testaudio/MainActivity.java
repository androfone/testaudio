package com.example.testaudio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static String DEBUG_TAG = "MainActivity";
    private String rate;
    private String size;
    private int playMinBufferSize;
    private int recMinBufferSize;
    private String unprocessed;
    private boolean hasLowLatencyFeature;
    private boolean hasProFeature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        rate = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
        size = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        int SAMPLE_RATE = Integer.parseInt(rate);
        int PREF_BUFFER_SIZE = Integer.parseInt(size);
        Log.d(DEBUG_TAG,"MAIN MASTER VALUE SAMPLE_RATE: " + rate);
        TextView t1 = findViewById(R.id.param1);
        t1.setText("MAIN MASTER VALUE SAMPLE_RATE: " + rate);
        Log.d(DEBUG_TAG,"MAIN MASTER VALUE PREF_BUFFER_SIZE(output): " + size);
        TextView t2 = findViewById(R.id.param2);
        t2.setText("MAIN MASTER VALUE PREF_BUFFER_SIZE(output): " + size);
        playMinBufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        Log.d(DEBUG_TAG, "AudioTrack getMinBufferSize: " + playMinBufferSize);
        TextView t3 = findViewById(R.id.param3);
        t3.setText("AudioTrack getMinBufferSize: " + playMinBufferSize);
        recMinBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        Log.d(DEBUG_TAG, "AudioRecord getMinBufferSize: " + recMinBufferSize);
        TextView t4 = findViewById(R.id.param4);
        t4.setText("AudioRecord getMinBufferSize: " + recMinBufferSize);
        unprocessed = audioManager.getProperty(AudioManager.PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED);
        Log.d(DEBUG_TAG, "PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED " + unprocessed);
        TextView t5 = findViewById(R.id.param5);
        t5.setText("PROPERTY_SUPPORT_AUDIO_SOURCE_UNPROCESSED " + unprocessed);
        PackageManager packageManager = getPackageManager();
        hasLowLatencyFeature =
                packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_LOW_LATENCY);
        hasProFeature =
                packageManager.hasSystemFeature(PackageManager.FEATURE_AUDIO_PRO);
        Log.d(DEBUG_TAG, "FEATURE_AUDIO_LOW_LATENCY " + hasLowLatencyFeature);
        Log.d(DEBUG_TAG, "FEATURE_AUDIO_PRO " + hasProFeature);
        TextView t6 = findViewById(R.id.param6);
        t6.setText("FEATURE_AUDIO_LOW_LATENCY " + hasLowLatencyFeature);
        TextView t7 = findViewById(R.id.param7);
        t7.setText("FEATURE_AUDIO_PRO " + hasProFeature);
    }


}